from django.shortcuts import get_object_or_404
from django.utils.datastructures import MultiValueDictKeyError
from rest_framework import viewsets
from rest_framework.response import Response

from smi_unb.buildings.models import Building
from smi_unb.transductor.models import EnergyTransductor, EnergyMeasurements
from .serializers import BuildingSerializer, EnergyTransductorSerializer, \
                         EnergyMeasurementsSerializer


class BuildingViewSet(viewsets.ModelViewSet):
    queryset = Building.objects.all()
    serializer_class = BuildingSerializer


class EnergyTransductorViewSet(viewsets.ModelViewSet):
    queryset = EnergyTransductor.objects.all()
    serializer_class = EnergyTransductorSerializer

    def create(self, request, *args, **kwargs):
        try:
            update_collection = self.request.query_params['update_collection']
        except MultiValueDictKeyError:
            return super(EnergyTransductorViewSet, self).create(
                request, *args, **kwargs)
        else:
            if update_collection == 'true':
                for transductor_data in request.data:
                    instance = get_object_or_404(
                        EnergyTransductor, id=transductor_data['id'])

                    energy_serializer = self.get_serializer(
                        instance=instance, data=transductor_data, partial=True)

                    energy_serializer.is_valid(raise_exception=True)
                    self.perform_update(energy_serializer)

            serializer = self.get_serializer(self.queryset, many=True)
            return Response(serializer.data)


class EnergyMeasurementsViewSet(viewsets.ModelViewSet):
    queryset = EnergyMeasurements.objects.all()
    serializer_class = EnergyMeasurementsSerializer

    def get_queryset(self):
        try:
            last_measurements = self.request.query_params['last']
        except MultiValueDictKeyError:
            self.queryset = EnergyMeasurements.objects.all()
        else:
            if last_measurements == 'true':
                self.queryset = []

                transductors = EnergyTransductor.objects.filter(active=True)

                for transductor in transductors:
                    measurements = transductor.get_last_measurements_or_all()
                    self.queryset += measurements
            else:
                self.queryset = EnergyMeasurements.objects.all()

        return self.queryset
