from rest_framework import serializers

from smi_unb.buildings.models import Building
from smi_unb.transductor.models import EnergyTransductor, EnergyMeasurements


class EnergyTransductorSerializer(serializers.ModelSerializer):
    def __init__(self, *args, **kwargs):
        many = kwargs.pop('many', True)
        super(EnergyTransductorSerializer, self).__init__(
            many=many, *args, **kwargs)

    id = serializers.IntegerField()

    class Meta:
        model = EnergyTransductor
        fields = (
            'id', 'building', 'model', 'active', 'ip_address',
            'name', 'serie_number', 'local_description',
            'comments', 'creation_date', 'calibration_date',
            'broken', 'last_measurement_sent')


class BuildingSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()

    class Meta:
        model = Building
        fields = (
            'id', 'campus', 'name', 'acronym', 'description', 'phone',
            'website_address', 'server_ip_address', 'active')


class EnergyMeasurementsSerializer(serializers.ModelSerializer):
    def __init__(self, *args, **kwargs):
        many = kwargs.pop('many', True)
        super(EnergyMeasurementsSerializer, self).__init__(
            many=many, *args, **kwargs)

    class Meta:
        model = EnergyMeasurements
        fields = (
            'collection_date', 'transductor',
            'voltage_a', 'voltage_b', 'voltage_c',
            'current_a', 'current_b', 'current_c',
            'active_power_a', 'active_power_b', 'active_power_c',
            'reactive_power_a', 'reactive_power_b', 'reactive_power_c',
            'apparent_power_a', 'apparent_power_b', 'apparent_power_c')
